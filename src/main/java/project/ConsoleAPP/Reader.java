package project.ConsoleAPP;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Reader   {

    public static ArrayList<String> fileReader(String getFile) {
        String gLst;
        ArrayList<String> getList = new ArrayList<String>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(getFile));
            while ((gLst = br.readLine()) !=null ) {
                getList.add(gLst);
                //System.out.println(gLst);
            }
        } catch (IOException e) {
            System.err.println("Ошибка чтения указанного файла: " + e);
        }
        return getList;
    }
}
