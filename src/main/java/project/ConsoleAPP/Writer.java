package project.ConsoleAPP;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Writer {
    public static void fileWriter(int number, String writeFile, ArrayList<String> getList) {
        int a = Integer.valueOf(number);
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(writeFile));
            for (int i = a - 1; i < getList.size(); i += a) {
                String c = getList.get(i) + "\r\n";
                bw.write(c);
                bw.flush();

              //  System.out.println(c);
            }
        }catch (IOException e) {
            System.err.println("Ошибка записи в файл: " +e);
        }
    }
}

