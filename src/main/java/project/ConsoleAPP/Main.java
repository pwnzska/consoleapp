package project.ConsoleAPP;

import java.util.ArrayList;


public class Main {


    public static void main(String[] args) {
        UserInteraction b = new UserInteraction();
        String rdFile;
        String wrFile;
        int number;
        /**
         * Параметры не переданы при запуске
         */
        if (args.length == 0) {
            rdFile = b.getFileForRead();
            wrFile = b.getFileForWrite();
            number = b.getInt();
            ArrayList<String> list = Reader.fileReader(rdFile);
            Writer.fileWriter(number, wrFile, list);
        } else {
            /**
             * Переданы все 3 параметра
             */
            if (args.length == 3 || args.length > 3) {
                if (args[0] == null) {
                    rdFile = b.getFileForRead();
                } else {
                    rdFile = b.getFileForRead(args[0]);
                }
                if (args[1] == null) {
                    wrFile = b.getFileForWrite();
                } else {
                    wrFile = b.getFileForWrite(args[1]);
                }
                if (args[2] == null) {
                    number = b.getInt();
                } else {
                    number = b.getInt(args[2]);
                }
                ArrayList<String> list = Reader.fileReader(rdFile);
                Writer.fileWriter(number, wrFile, list);
            }
            /**
             * Передано только 2 параметра
             */
            if (args.length == 2) {
                if (args[0] == null) {
                    rdFile = b.getFileForRead();
                } else {
                    rdFile = b.getFileForRead(args[0]);
                }
                if (args[1] == null) {
                    wrFile = b.getFileForWrite();

                } else {
                    wrFile = b.getFileForWrite(args[1]);
                }
                number = b.getInt();
                ArrayList<String> list = Reader.fileReader(rdFile);
                Writer.fileWriter(number, wrFile, list);
            }
            /**
             * Передан только 1 параметр
             */
            if (args.length == 1) {
                if (args[0] == null) {
                    rdFile = b.getFileForRead();
                } else {
                    rdFile = b.getFileForRead(args[0]);
                }
                wrFile = b.getFileForWrite();
                number = b.getInt();
                ArrayList<String> list = Reader.fileReader(rdFile);
                Writer.fileWriter(number, wrFile, list);
            }
        }
    }




}
