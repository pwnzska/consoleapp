package project.ConsoleAPP;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class UserInteraction {
    private static Scanner sr = new Scanner(System.in);

    /**
     * Метод для чтения из консоли названия/пути файла с которого читаем
     */
    static String getFileForRead() {
        System.out.println("Enter the path for reading file");
        String fileName = checkFileForRead(consoleReader());
        return fileName;
    }

    /**
     * Метод для чтения из консоли названия/пути файла с которого читаем
     */
    static String getFileForRead(String x) {
        String fileName = checkFileForRead(x);
        return fileName;
    }

    /**
     * Метод проверяющий существование файла с которого читаем
     */
    private static String checkFileForRead(String x) {
            while (true) {
                File file = new File(x);
                if ((file.exists()) && (file.isFile())) {
                    return x;
                } else {
                    System.out.println("Wtf, file for reading doesn't exist");
                    x = getFileForRead();
                    return x;
                }
            }
    }

    /**
     * Метод для чтения из консоли названия/пути файла с которого читаем
     */
    static String getFileForWrite() {
        System.out.println("Enter the path or filename for writing file");
        String fileName = checkFileForWrite(consoleReader());
        return fileName;
    }

    /**
     * Метод для чтения из консоли названия/пути файла с которого читаем
     */
    static String getFileForWrite(String x) {
        String fileName = checkFileForWrite(x);
        return fileName;
    }

    /**
     * Метод для проверки названия файла в который записываем
     */
    private static String checkFileForWrite(String tmp2) {
            while (true) {
                File file = new File(tmp2);
                try {
                    boolean created = file.createNewFile();
                    if (created || file.exists()) {
                        if (file.isFile()) {
                            return tmp2;
                        }
                        else {
                            System.out.println("It's not a file");
                            tmp2 = getFileForWrite();
                        }
                    }
                    else {System.out.println("Incorrect filename or path for writing file");}
                } catch (IOException e) {
                    System.out.println("Incorrect filename or path for writing file");
                    tmp2 = getFileForWrite();
                    return tmp2;
                }
            }
    }

    /**
     * Метод для чтения номера нужных нам строк из консоли
     */
    static int getInt() {
        System.out.println("Enter the number of necessary strings ");
        String tmp3 = consoleReader();
        int number = intHandler(tmp3);
        return number;

    }

    /**
     * Метод для чтения номера нужных нам строк
     */
    static int getInt(String x) {
        String tmp3 = x;
        int number = intHandler(tmp3);
        return number;
    }

    /**
     * Метод для проверки номера нужных нам строк
     */
    private static int intHandler(String x) {
        int number = 0;
            boolean getint = true;
            while (getint) {
                try {
                    number = Integer.valueOf(x);
                    if (!(number <= 0)) {
                        getint = false;
                    } else {
                        System.out.println("Number must be more than 0");
                        number = getInt();
                        return number;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("it's bad number, try again");
                    number = getInt();
                    return number;
                }
            }
        return number;
    }

    private static String consoleReader() {
        String string = sr.nextLine();
        if (string.equals("exit")) {
            System.exit(-1);
        }
        return string;
    }
}
